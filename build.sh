#!/bin/bash
############################################
# Function :  Docker镜像制作脚本
# Author : tang
# Date : 2021-07-04
#
# Usage: sh build.sh
#
############################################

docker build -t inrgihc/greenplum:6.19.3 .
